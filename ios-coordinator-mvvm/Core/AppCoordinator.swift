import Foundation
import UIKit

class AppCoordinator: RootViewCoordinator {

    let window: UIWindow

    var childCoordinators: [Coordinator] = []
    var rootViewController: UINavigationController {
        return self.navigationController
    }

    private lazy var navigationController: UINavigationController = {
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        return navigationController
    }()

    init(window: UIWindow) {
        self.window = window

        self.window.rootViewController = self.rootViewController
        self.window.makeKeyAndVisible()
    }

    func start() {
        pushInitialViewController()
    }

    private func pushInitialViewController() {
        let initialViewController = InitialViewController()

        DispatchQueue.main.async {
            self.rootViewController.pushViewController(initialViewController, animated: true)
        }
    }
}
