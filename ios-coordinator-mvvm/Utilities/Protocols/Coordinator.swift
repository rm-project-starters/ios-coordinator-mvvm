import Foundation

public protocol Coordinator: class {
    func start()
    var childCoordinators: [Coordinator] { get set }
}

extension Coordinator {
    func addChildCoordinator(childCoordinator: Coordinator) {
        self.childCoordinators.append(childCoordinator)
    }

    func removeChildCoordinator(childCoordinator: Coordinator) {
        self.childCoordinators = self.childCoordinators.filter({ $0 !== childCoordinator })
    }
}
